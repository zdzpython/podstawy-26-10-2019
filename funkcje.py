import operator

def pierwiastek(a,b):
    return a**(1/(b*1.))

mathop = {'+':operator.add,
          '-':operator.sub,
          '*':operator.mul,
          '/':operator.truediv,
          '%':operator.mod,
          'p':pierwiastek}

#zm1=pierwiastek(9,2)
#print("FUNKCJA PIERWIASTEK I PRZYPISANA WARTOSC DO zm1",zm1)
def wielodzialanie(*a,op='+'):
    wynik=0
    if (op!='+' and op!='-'):
        wynik+=1 # <- wynik=wynik+1
    for i in a:
        wynik=mathop[op](wynik,i)
    print(f'{a} {op} = {wynik}')

def dzialaj(a,b,op='+'):
    dzialanie="Sumowanie"
    #if(op=='+'):
        
    if(op=='-'):
        dzialanie="Odejmowanie"
    elif(op=='*'):
        dzialanie="Mnożenie"
    elif(op=='/'):
        dzialanie="Dzielenie"
    elif(op=='%'):
        dzialanie="Modulo"
    elif(op=='p'):
        dzialanie="Pierwiastek"
    print(f"{dzialanie} {a} oraz {b} wynosi",mathop[op](a,b))


print("Witaj w programie, zobacz sumowanie dwóch liczb")

dzialaj(3,5)
print("Sumuj wielokrotnie")
dzialaj(-90,786,'%')
dzialaj(9,11,'*')
dzialaj(98,-100)
dzialaj(4,3,'%')
dzialaj(144,2,'p')

wielodzialanie(13,14,5,6,7,-90,op='*')
wielodzialanie(13,14,5,6,7,-90,op='-')
wielodzialanie(13,14,5,6,7,-90)






