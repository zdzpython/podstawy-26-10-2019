#przykład wyświetlenia tekstu w języku Python; wyświetlenie cydzysłowia
#w przypadku użycia cudzysłowia jako ogranicznika ciągu znakowego wymaga
#wykorzystania ukośnika, tzw. backslash, który to informuje parser, że 
#występujący po nim znak ma nie być traktowany jako znak językowy (w poniższym wypadku
#zamknięcie ciągu znakowego) lecz jako jeden ze znaków do wyświetlenia
#Ponadto w funkcji print wykorzystany został parametr sep który pozwala na 
#zmianę domyślnego rozdzialania kolejnych części ciągu znakowego (domyślnie jest to spacja
#poniższy przykład zmienia spację na brak jakiegokolwiek znaku)
print("Firma nazywa się \"Kia\", a znak specjalny",
      "\\ wyświetla się przez jego podwójne podanie",
      "w ciągu znakowym",sep="")
#Tekst w Python można łączyć - poniżej przykład łączenia za pomocą znaków plus
print(("Łączenie tekstu za pomocą"+"łącznika +")*2)

imie,wiek,liczba=input("Podaj imię "),int(input("Podaj wiek")),int(input("Podaj liczbę"))
print("Sposób pierwszy")

#Poniżej przedstawione są różne sposoby wyświetalania w języku Python 
print("Twoje imię to",imie,". Masz",wiek,"lat. Twoja szczęśliwa liczba to",liczba,".")
print("Sposób drugi")
#styl C/C++
print("Twoje imię to %s. Masz %d lat. Twoja szczęśliwa liczba to %x." % (imie,wiek,liczba)) 
print("Trzeci sposób")
#styl Python 2.7
print("Twoje imię to %(nazwa)s. Masz %(lata)d lat. Twoja szczęśliwa liczba to %(numer)x (dziesiętnie %(numer)d)." % {"nazwa": imie,"lata": wiek,"numer": liczba })
print("Czwarty sposób")
#formatowanie od Python 3.x
print(f"Twoje imię to {imie}. Masz {wiek} lat. Twoja szczęśliwa liczba to {liczba} (szesnastkowo {liczba:x}).")
