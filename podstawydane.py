#Podstawowa funkcja w języku Python pozwalająca na wyświetlenie dowolnych infomracji na konsoli dla użytkownika
#Poniższa funkcja mogła zostać zapisana w taki sposób:
#print("Witaj","w świecie","języka Python",", który jest inny niż wszystkie pozostałe",end="!\n",sep=" - ")
#jednak wybrany poniżej zapis jest bardziej czytelny (każdy parametr funkcji podany jest w nowej linii)
#Charakterystyczne jest to, że funkcja może przyjąć dowolną ilość argumentów. Każdy z nich zostanie wyświetlony/wypisany do pliku
#Python pozwala na przypisaywanie parametrom dowolnych wartości (jak C++) oraz pozwala je pomijać/zmieniać im kolejność;
#Każdy parametr w funkcji można wywołać poprzez jego nazwę i poprzez znak = przypisać nową wartość
#W poniższe funkcji tak zostały przemienione wartości paramwtru end oraz sep
print("Witaj",
      "w świecie",
      "języka Python",
      ", który jest inny niż wszystkie pozostałe",
      end="!\n",
      sep=" - ")
#Utworzenie nowej zmiennej nie wymaga żadnego słowa kluczowego; podajemy jej nazwę i po znaku = przypisujemy jej dowolną wartość
a=5
#Python sam wykryje, czy mamy do czynienia z liczbami, czy może z tekstem 
text="Nowy tekst do wyświeltenia"
#funkcja print pozwala na wyświetlanie różnego rodzaju zmiennych 
print(text,a)
#wcześniej zdefiniowanan zmienna a zmienia swoją wartość z liczby 5 na tekst, który ma wpisany liczbę 5 (inne wartości binarne!)
a="5"
#Przykład pobierania danych od użytkownika z lini poleceń; pobraną liczbę od razu zamieniamy na
#wartość liczbową (int), gdyż Python wszystko wczytane z linii poleceń zamienia na ciąg znakowy 
liczba1=int(input("Wpisz pierwszą liczbę"))
liczba2=int(input("Wpisz drugą liczbę"))
#wyświetlenie wczytanych liczb i podanie wyniku ich wymnożenia jako kolejne parametry funkcji print
print(liczba1,"*",liczba2,"=",liczba1*liczba2)
#Python umożliwia szeregowe przypisywanie wartości do zmiennych; wartości można modyfikować wedle potrzeb 
liczba3,liczba4=liczba2,liczba1+9
print(liczba3,", ",liczba4)

zmienna=float(input("Podaj zmienną z przecinkiem"))

print(zmienna)
#liczby zmiennoprzecinkowe możemy zapisywać z oznaczniem eksponecjonalnym
#dzięki temu zabiegowi możliwe jest zapisanie bardzo małych liczb (po przecinku)
#w postaci całkowitej oraz ich potęgi liczby 10 (w poniższym przypadku 10^-24 czyli
#przesunięcie przecinka o 24 miejsca W LEWĄ STRONĘ!)
eksp=46433632672267277e-24
print(eksp)